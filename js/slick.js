  $(function(){
    $('.testimonials__slick').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      asNavFor: '.testimonials__slick-text',
      responsive:[
        {
          breakpoint: 700,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });
    $('.testimonials__slick-text').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      dots: true,
      arrows: false,
      asNavFor: '.testimonials__slick'
   });
  });